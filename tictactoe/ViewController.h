//
//  ViewController.h
//  tictactoe
//
//  Created by Alain Pitre on 2016-01-08.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *s0;
@property (weak, nonatomic) IBOutlet UIButton *s1;
@property (weak, nonatomic) IBOutlet UIButton *s2;
@property (weak, nonatomic) IBOutlet UIButton *s3;
@property (weak, nonatomic) IBOutlet UIButton *s4;
@property (weak, nonatomic) IBOutlet UIButton *s5;
@property (weak, nonatomic) IBOutlet UIButton *s6;
@property (weak, nonatomic) IBOutlet UIButton *s7;
@property (weak, nonatomic) IBOutlet UIButton *s8;

@property (weak, nonatomic) IBOutlet UIButton *restart;

- (IBAction)restart:(id)sender;

- (IBAction)play:(id)sender;

@end

