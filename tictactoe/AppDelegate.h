//
//  AppDelegate.h
//  tictactoe
//
//  Created by Alain Pitre on 2016-01-08.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

