//
//  Game.m
//  tictactoe
//
//  Created by Alain Pitre on 2016-01-10.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import "Game.h"

@implementation Game {
    NSString *players[2];
    int current;
    UIButton *buttons[3][3];
    int win;
    int hit;
}

static int options[8][3][2] = {
    {{0,0}, {0,1}, {0,2}},
    {{1,0}, {1,1}, {1,2}},
    {{2,0}, {2,1}, {2,2}},
    {{0,0}, {1,0}, {2,0}},
    {{0,1}, {1,1}, {2,1}},
    {{0,2}, {1,2}, {2,2}},
    {{0,0}, {1,1}, {2,2}},
    {{2,0}, {1,1}, {0,2}}
};

- (void) construct {
    players[0]  = @"x";
    players[1]  = @"o";
    current     = 0;
    win         = -1;
    hit         = 0;
    
    UIButton *tempBtn;
    
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            tempBtn = [self getButton:x :y];
            UIColor *white = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.25f];
            [tempBtn setBackgroundColor:white];
            [tempBtn setTitle:@"" forState:UIControlStateNormal];
        }
    }
}

- (void) addButton: (UIButton *)button :(int)x :(int)y {
    button.layer.cornerRadius = 30;
    buttons[x][y] = button;
}

- (UIButton*) getButton: (int)x :(int)y {
    return buttons[x][y];
}

- (int) hasLine {
    
    int count = 0;
    UIButton *button;
    
    for (int i = 0; i < 8; i++) {
        
        for (int y = 0; y < 3; y++) {
            button = [self getButton:options[i][y][0] :options[i][y][1]];
            if ([players[current] isEqualToString:[button currentTitle]]) {
                count++;
            }
        }
        
        if (count == 3) {
            return i;
        }
        
        count = 0;
        
    }
    
    return -1;
}

- (void) highlight {
    UIButton *tempBtn;
    for (int i = 0; i < 3; i++) {
        tempBtn = [self getButton:options[win][i][0] :options[win][i][1]];
        UIColor *higlight = [UIColor colorWithRed:255/255.0f green:50/255.0f blue:60/255.0f alpha:1.0f];
        [tempBtn setBackgroundColor:higlight];
    }
}

- (BOOL) isAvailable: (UIButton*) button {
    return ([[button currentTitle] isEqualToString:@""]) && (win < 0);
}

- (void) finish {
    UIAlertView *alert = [[UIAlertView alloc]
                            initWithTitle       : @"Game Finished"
                            message             : @"Restart Tic Tac Toe"
                            delegate            : self
                            cancelButtonTitle   : nil
                            otherButtonTitles   : @"OK",nil
                          ];
    [alert setTag:1];
    [alert show];
    
    if (win >= 0) {
        [self highlight];
    }
    
}

- (void) updatePlayer {
    hit++;
    win = [self hasLine];
    
    if ((win >= 0) || (hit == 9)) {
        [self finish];
    }else if (current == 0) {
        current = 1;
    } else {
        current = 0;
    }
}

- (NSString*) getCurrent {
    return players[current];
}

@end