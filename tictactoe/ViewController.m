//
//  ViewController.m
//  tictactoe
//
//  Created by Alain Pitre on 2016-01-08.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"

@implementation ViewController {
    Game *game;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    game = [[Game alloc] init];
    
    [game addButton:_s0 :0 :0];
    [game addButton:_s1 :0 :1];
    [game addButton:_s2 :0 :2];
    [game addButton:_s3 :1 :0];
    [game addButton:_s4 :1 :1];
    [game addButton:_s5 :1 :2];
    [game addButton:_s6 :2 :0];
    [game addButton:_s7 :2 :1];
    [game addButton:_s8 :2 :2];
    
    [game construct];
}

- (IBAction)restart:(id)sender {
    [game construct];
}

- (IBAction)play:(id)sender {
    if ([game isAvailable:sender]) {
        [sender setTitle:[game getCurrent] forState:UIControlStateNormal];
        [game updatePlayer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
