//
//  Game.h
//  tictactoe
//
//  Created by Alain Pitre on 2016-01-10.
//  Copyright © 2016 Alain Pitre. All rights reserved.
//

#ifndef Game_h
#define Game_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Game : NSObject

- (void) construct;

- (UIButton*) getButton: (int)x :(int)y;

- (void) updatePlayer;

- (BOOL) isAvailable: (UIButton*) button;

- (NSString*) getCurrent;

- (void) addButton: (UIButton *)button :(int)x :(int)y;

@end


#endif /* Game_h */
